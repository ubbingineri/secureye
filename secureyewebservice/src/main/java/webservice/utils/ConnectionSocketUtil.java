package webservice.utils;

import java.io.IOException;
import java.net.Socket;

public class ConnectionSocketUtil {
    private Socket socket;

    private void initialize() {
        try {
            socket = new Socket("127.0.0.1", 4567);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ConnectionSocketUtil() {
        initialize();
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }
}
