package webservice.service.impl;

import webservice.service.IUserService;
import webservice.utils.ConnectionSocketUtil;

import java.io.*;

public class UserServiceImpl implements IUserService {
    private ConnectionSocketUtil connectionSocketUtil;

    public UserServiceImpl() {
        this.connectionSocketUtil = new ConnectionSocketUtil();
    }

    public UserServiceImpl(ConnectionSocketUtil connectionSocketUtil) {
        this.connectionSocketUtil = connectionSocketUtil;
    }

    @Override
    public String processCreditCard(String username, String creditCard) {
        try {
            ObjectOutput objectOutput = new ObjectOutputStream(connectionSocketUtil.getSocket().getOutputStream());
            objectOutput.writeObject(username);
            objectOutput.writeObject(creditCard);
            objectOutput.flush();

            ObjectInput objectInput = new ObjectInputStream(connectionSocketUtil.getSocket().getInputStream());
            String result = (String) objectInput.readObject();

            if (result.equals("SUCCESS")) {
                connectionSocketUtil.getSocket().close();
                return "SUCCESS";
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            connectionSocketUtil.getSocket().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "FAILURE";
    }
}
