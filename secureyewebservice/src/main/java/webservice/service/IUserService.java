package webservice.service;

public interface IUserService {

    String processCreditCard(String username, String creditCard);
}
