package desktop.service;

import desktop.model.User;

import java.sql.Timestamp;

public interface IUserService {

    User getUser(String pin, String creditCard, Timestamp timestamp, String inputDuration, Boolean successfulAuthentication);

    User getUser(String pin, String creditCard);
}
