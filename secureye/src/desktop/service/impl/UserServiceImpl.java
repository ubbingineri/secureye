package desktop.service.impl;

import desktop.model.User;
import desktop.model.UserStatistics;
import desktop.repository.IUserRepository;
import desktop.repository.IUserStatisticsRepository;
import desktop.repository.impl.UserRepositoryImpl;
import desktop.repository.impl.UserStatisticsRepositoryImpl;
import desktop.service.IUserService;

import java.sql.Timestamp;

public class UserServiceImpl implements IUserService {
    private IUserRepository userRepository;

    private IUserStatisticsRepository userStatisticsRepository;

    public UserServiceImpl() {
        this.userRepository = new UserRepositoryImpl();
        this.userStatisticsRepository = new UserStatisticsRepositoryImpl();
    }

    public UserServiceImpl(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUser(String pin, String creditCard) {

        return userRepository.getUser(pin, creditCard);
    }

    public User getUser(String pin, String creditCard, Timestamp timestamp, String inputDuration, Boolean successfulAuthentication) {
        User user = userRepository.getUser(pin, creditCard);

        if (user != null) {
            UserStatistics userStatistics = new UserStatistics(user.getId(), timestamp, inputDuration, pin,
                    true);
            userStatisticsRepository.save(userStatistics);
        }

        return user;
    }

    public IUserRepository getUserRepository() {
        return userRepository;
    }

    public void setUserRepository(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public IUserStatisticsRepository getUserStatisticsRepository() {
        return userStatisticsRepository;
    }

    public void setUserStatisticsRepository(IUserStatisticsRepository userStatisticsRepository) {
        this.userStatisticsRepository = userStatisticsRepository;
    }
}
