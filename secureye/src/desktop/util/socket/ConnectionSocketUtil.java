package desktop.util.socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ConnectionSocketUtil {
    private ServerSocket serverSocket;
    private int port;

    public ConnectionSocketUtil(int port) {
        this.port = port;
    }

    public void start() {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            System.out.println("Could not listen to port: " + e);
        }

        System.out.println("Server successfully started.");

        while (true) {
            try {
                Socket socket = serverSocket.accept();

                new Thread(new CreditCardHandler(socket)).start();
            } catch (IOException e) {
                System.out.println("Error accepting socket: " + e);
            }
        }
    }

    public ServerSocket getServerSocket() {
        return serverSocket;
    }

    public void setServerSocket(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
