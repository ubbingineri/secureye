package desktop.model;

import java.sql.Timestamp;

public class UserStatistics {
    private Integer userId;

    private Timestamp timestamp;

    private String inputDuration;

    private String inputPin;

    private Boolean successfulAuthentication;

    public UserStatistics(Timestamp timestamp, String inputDuration, String inputPin, Boolean successfulAuthentication) {
        this.timestamp = timestamp;
        this.inputDuration = inputDuration;
        this.inputPin = inputPin;
        this.successfulAuthentication = successfulAuthentication;
    }

    public UserStatistics(Integer userId, Timestamp timestamp, String inputDuration, String inputPin, Boolean successfulAuthentication) {
        this.userId = userId;
        this.timestamp = timestamp;
        this.inputDuration = inputDuration;
        this.inputPin = inputPin;
        this.successfulAuthentication = successfulAuthentication;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getInputDuration() {
        return inputDuration;
    }

    public void setInputDuration(String inputDuration) {
        this.inputDuration = inputDuration;
    }

    public String getInputPin() {
        return inputPin;
    }

    public void setInputPin(String inputPin) {
        this.inputPin = inputPin;
    }

    public Boolean getSuccessfulAuthentication() {
        return successfulAuthentication;
    }

    public void setSuccessfulAuthentication(Boolean successfulAuthentication) {
        this.successfulAuthentication = successfulAuthentication;
    }
}
