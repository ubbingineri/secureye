package desktop.repository.impl;

import desktop.model.User;
import desktop.util.persistence.IDatabaseConnection;
import desktop.util.persistence.impl.DatabaseConnectionImpl;
import desktop.repository.IUserRepository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UserRepositoryImpl implements IUserRepository {
    private IDatabaseConnection databaseConnection;

    public UserRepositoryImpl() {
        databaseConnection = new DatabaseConnectionImpl();
    }

    public UserRepositoryImpl(IDatabaseConnection databaseConnection) {
        this.databaseConnection = databaseConnection;
    }

    @Override
    public User getUser(String pin) {
        return null;
    }

    @Override
    public User getUser(String pin, String creditCard) {
        databaseConnection.open();
        Connection connection = null;
        User user = null;

        try {
            connection = databaseConnection.getConnection();

            String select = "SELECT * FROM users WHERE pin='" + pin + "' AND creditCard='" + creditCard + "' LIMIT 1";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(select);

            while (resultSet.next()) {
                user = new User(resultSet.getInt("id"), resultSet.getString("username"), resultSet.getString("pin"),
                        resultSet.getString("creditCard"), Integer.parseInt( resultSet.getString("currentAmount")));
            }

            return user;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return user;
    }
}
