package desktop.repository;

import desktop.model.User;

public interface IUserRepository {
    User getUser(String pin);

    User getUser(String pin, String creditCard);
}
