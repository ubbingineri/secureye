package desktop.repository;

import desktop.model.UserStatistics;

import java.util.List;

public interface IUserStatisticsRepository {

    List<UserStatistics> get();

    void save(UserStatistics userStatistics);

    void update(UserStatistics userStatistics);

    void delete(UserStatistics userStatistics);
}
