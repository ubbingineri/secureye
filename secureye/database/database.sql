CREATE TABLE users(
id int primary key auto_increment,
username varchar(500),
pin varchar(250),
creditCard varchar(250)
);

INSERT INTO users(id, username, pin, creditCard) VALUES(1, 'Ion', '1234', 'card');

CREATE TABLE userstatistics(
userId int primary key,
timestamp timestamp,
inputDuration timestamp,
inputPin varchar(250),
successfulAuthentication bit
);
